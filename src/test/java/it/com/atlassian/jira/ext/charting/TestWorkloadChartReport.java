package it.com.atlassian.jira.ext.charting;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.navigation.IssueNavigation;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Integration Test Case for workload pie chart
 */
@Restore("TestWorkloadChartReport.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkloadChartReport extends AbstractChartTestCase {
    @Inject
    protected Administration administration;

    @Inject
    protected IssueNavigation issueNavigation;

    @Inject
    protected TimeTracking timeTracking;

    protected void configureReport(final String projectOrFilter, final String statisticsType, final String issueTimeType) {
        selectProjectOrFilterId(projectOrFilter);
        tester.selectOption("statistictype", statisticsType);
        tester.selectOption("issuetimetype", issueTimeType);
        tester.submit("Next");
    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByAssigneeAndTimeSpent() {
        timeTracking.enable(TimeTracking.Mode.MODERN);
        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", "1w", ADMIN_USERNAME, "1d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", "2w", TEST_USERNAME, "2d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug", "High", "1w", TEST_USERNAME, "1d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Fourth Bug", "High", "1w", TEST_USERNAME, "1d", null, null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");

        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{TEST_USERNAME, "32", "80%"},
                        new String[]{"admin", "8", "20%"}
                });
    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByAssigneeAndCurrentEstimate() {
        timeTracking.enable(TimeTracking.Mode.MODERN);
        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", "1w", ADMIN_USERNAME, "1d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", "2w", TEST_USERNAME, "2d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug", "High", "1w", TEST_USERNAME, "1d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Fourth Bug", "High", "1w", TEST_USERNAME, "1d", null, null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Current Estimate");

        tester.assertTextPresent("Workload Pie Chart Report");

        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{TEST_USERNAME, "128", "80%"},
                        new String[]{"admin", "32", "20%"}

                });
    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByAssigneeAndOriginalEstimate() {
        timeTracking.enable(TimeTracking.Mode.MODERN);
        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", "1w", ADMIN_USERNAME, "1d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", "2w", TEST_USERNAME, "2d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug", "High", "1w", TEST_USERNAME, "1d", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Fourth Bug", "High", "1w", TEST_USERNAME, "1d", null, null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Original Estimate");

        tester.assertTextPresent("Workload Pie Chart Report");

        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{TEST_USERNAME, "160", "80%"},
                        new String[]{"admin", "40", "20%"}

                });
    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByAllFixForVersions() {
        timeTracking.enable(TimeTracking.Mode.MODERN);

        administration.project().addVersion(TEST_PROJECT_KEY, "1.0", null, null);
        administration.project().addVersion(TEST_PROJECT_KEY, "2.0", null, null);

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", null, TEST_USERNAME, "5h", "1.0", null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", null, TEST_USERNAME, "15h", "2.0", null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        administration.project().archiveVersion(TEST_PROJECT_KEY, "1.0");

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Fix For Versions (all)", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"2.0", "15", "75%"},
                        new String[]{"1.0", "5", "25%"}
                });
    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByNonArchivedFixForVersions() {
        timeTracking.enable(TimeTracking.Mode.MODERN);

        administration.project().addVersion(TEST_PROJECT_KEY, "1.0", null, null);
        administration.project().addVersion(TEST_PROJECT_KEY, "2.0", null, null);
        administration.project().addVersion(TEST_PROJECT_KEY, "3.0", null, null);

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", null, TEST_USERNAME, "5h", "2.0", null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", null, TEST_USERNAME, "15h", "3.0", null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug", "High", null, ADMIN_USERNAME, "500h", "1.0", null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        administration.project().archiveVersion(TEST_PROJECT_KEY, "1.0"); // Third Bug should be excluded from the report

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Fix For Versions (non-archived)", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"3.0", "15", "75%"},
                        new String[]{"2.0", "5", "25%"}
                });
    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByComponent() {
        timeTracking.enable(TimeTracking.Mode.MODERN);

        administration.project().addComponent(TEST_PROJECT_KEY, "Component 1", StringUtils.EMPTY, ADMIN_USERNAME);
        administration.project().addComponent(TEST_PROJECT_KEY, "Component 2", StringUtils.EMPTY, ADMIN_USERNAME);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "High");
            issueNavigation.setComponents(issueKey, "Component 1");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, TEST_USERNAME);
            issueNavigation.logWork(issueKey, "5h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "High");
            issueNavigation.setComponents(issueKey, "Component 2");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, TEST_USERNAME);
            issueNavigation.logWork(issueKey, "15h");
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Components", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"Component 2", "15", "75%"},
                        new String[]{"Component 1", "5", "25%"}
                });
    }

    @Test
    public void testGenerateWorkloadPieChartNoTimeTracking() {
        administration.project().addComponent(TEST_PROJECT_KEY, "Component 1", StringUtils.EMPTY, ADMIN_USERNAME);
        administration.project().addComponent(TEST_PROJECT_KEY, "Component 2", StringUtils.EMPTY, ADMIN_USERNAME);
        timeTracking.disable();

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", null, TEST_USERNAME, null, null, "Component 1");
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", null, TEST_USERNAME, null, null, "Component 2");
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Components", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        tester.assertTextPresent("Please enable Jira time tracking");
    }

    @Test
    public void testGenerateWorkloadPieChartNoWorklog() {
        timeTracking.enable(TimeTracking.Mode.MODERN);

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", null, TEST_USERNAME, null, null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug", "High", null, TEST_USERNAME, null, null, null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        tester.assertTextPresent("no work logged");

    }

    @Test
    public void testGenerateWorkloadPieChartGroupedByIssueType() {
        timeTracking.enable(TimeTracking.Mode.MODERN);

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug", "High", null, TEST_USERNAME, "5h", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_NEWFEATURE, "Feature X", "High", null, TEST_USERNAME, "15h", null, null);
            createTestIssue(TEST_PROJECT_NAME, ISSUE_TYPE_IMPROVEMENT, "Improvement Y", "High", null, ADMIN_USERNAME, null, null, null);
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Issue Type", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"New Feature", "15", "75%"},
                        new String[]{"Bug", "5", "25%"},
                        new String[]{"Improvement", "0", "0%"}
                });
    }

    @Test
    public void testGenerateWorkloadPieChartWithCustomFieldShowingProperLabel() {
        String customFieldLabel = "Labelss";

        String customFieldId = administration.customFields().addCustomField(
                "com.atlassian.jira.plugin.system.customfieldtypes:labels", customFieldLabel);

        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "High");
        } finally {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, customFieldLabel, "Time Spent");

        tester.assertTextPresent("by " + customFieldLabel);
        tester.assertTextNotPresent("by " + customFieldId);
    }

    private String createTestIssue(String projectName, String issueType, String summary, String priority, String estimate, String assignee, String worklogTime, String fixVersion, String component) {
        String issueKey = issueNavigation.createIssue(projectName, issueType, summary);
        if (StringUtils.isNotEmpty(priority)) {
            issueNavigation.setPriority(issueKey, priority);
        }
        if (StringUtils.isNotEmpty(estimate)) {
            issueNavigation.setEstimates(issueKey, estimate, StringUtils.EMPTY);
        }

        if (StringUtils.isNotEmpty(assignee)) {
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, assignee);
        }

        if (StringUtils.isNotEmpty(worklogTime)) {
            issueNavigation.logWork(issueKey, worklogTime);
        }

        if (StringUtils.isNotEmpty(fixVersion)) {
            issueNavigation.setFixVersions(issueKey, fixVersion);
        }

        if (StringUtils.isNotEmpty(component)) {
            issueNavigation.setComponents(issueKey, component);
        }
        return issueKey;
    }
}
