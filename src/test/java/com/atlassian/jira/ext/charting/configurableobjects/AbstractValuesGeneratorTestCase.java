package com.atlassian.jira.ext.charting.configurableobjects;

import com.atlassian.configurable.ValuesGenerator;
import junit.framework.TestCase;

public abstract class AbstractValuesGeneratorTestCase extends TestCase {

    protected abstract ValuesGenerator getValuesGenerator();
}
