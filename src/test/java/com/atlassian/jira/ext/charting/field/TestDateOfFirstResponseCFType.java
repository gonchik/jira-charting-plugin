package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverterImpl;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.rest.Dates;
import com.atlassian.jira.security.JiraAuthenticationContext;
import junit.framework.TestCase;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyCollection;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class TestDateOfFirstResponseCFType extends TestCase
{
    private long firstCommentTime = System.currentTimeMillis();

    private DatePickerConverter datePickerConverter;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private CustomFieldValuePersister customFieldValuePersister;

    @Mock
    private DateTimeFormatterFactory dateTimeFormatterFactory;

    @Mock
    private DateTimeFormatter dateTimeFormatter;
    
    @Mock
    private CustomField customField;

    @Mock
    private Issue issue;

    @Mock
    private DateOfFirstResponseDAO dateOfFirstResponseDAO;

    private DateOfFirstResponseCFType dateOfFirstResponseCFType;

    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        when(dateTimeFormatterFactory.formatter()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.forLoggedInUser()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withStyle(Matchers.<DateTimeStyle>anyObject())).thenReturn(dateTimeFormatter);

        datePickerConverter = new DatePickerConverterImpl(jiraAuthenticationContext, null);

        dateOfFirstResponseCFType = new DateOfFirstResponseCFType(datePickerConverter, customFieldValuePersister, new DateTimeFormatterFactoryStub()) {
            @Override
            protected DateOfFirstResponseDAO createDateOfFirstResponseDAO() {
                return dateOfFirstResponseDAO;
            }
        };
    }

    private DateOfFirstResponseCFType getDateOfFirstResponseCFType(
            final DatePickerConverter datePickerConverter,
            final CustomFieldValuePersister customFieldValuePersister,
            final DateTimeFormatterFactory dateTimeFormatterFactory)
    {
        return new DateOfFirstResponseCFType(datePickerConverter, customFieldValuePersister, dateTimeFormatterFactory)
        {
            @Override
            protected Timestamp calculateFirstResponseDate(Issue issue)
            {
                return new Timestamp(firstCommentTime);
            }

            @Override
            protected DateOfFirstResponseDAO createDateOfFirstResponseDAO()
            {
                return new DateOfFirstResponseDAO(null)
                {

                    protected String getTableName(String entityName)
                    {
                        return entityName;
                    }

                    protected String getColName(String entityName, String fieldName)
                    {
                        return entityName + "." + fieldName;
                    }
                };
            }
        };
    }

    // https://studio.plugins.atlassian.com/browse/JCHART-357
    public void testGettingFirstResponseDateDoesNotStoreItInTheDatabase()
    {
        assertEquals(new Timestamp(firstCommentTime), getDateOfFirstResponseCFType(datePickerConverter, customFieldValuePersister, dateTimeFormatterFactory).getValueFromIssue(customField, issue));
        verify(customFieldValuePersister, never()).updateValues(Matchers.<CustomField>anyObject(), anyLong(), eq(PersistenceFieldType.TYPE_DATE), anyCollection());
    }

    public void testDateFormattersProvidedInVelocityContext()
    {
        DateTimeFormatter titleFormatter = mock(DateTimeFormatter.class);
        when(dateTimeFormatter.withStyle(DateTimeStyle.COMPLETE)).thenReturn(titleFormatter);

        DateTimeFormatter iso8601Formatter = mock(DateTimeFormatter.class);
        when(dateTimeFormatter.withStyle(DateTimeStyle.ISO_8601_DATE_TIME)).thenReturn(iso8601Formatter);

        Map<String, Object> contextMap = getDateOfFirstResponseCFType(datePickerConverter, customFieldValuePersister, dateTimeFormatterFactory).getVelocityParameters(null, null, null);
        
        assertSame(dateTimeFormatter, contextMap.get("datePickerFormatter"));
        assertSame(titleFormatter, contextMap.get("titleFormatter"));
        assertSame(iso8601Formatter, contextMap.get("iso8601Formatter"));
    }

    public void testJsonIsEmptyWhenValueIsNull() {
        when(dateOfFirstResponseDAO.calculateDate(any(Issue.class))).thenReturn(null);
        assertEquals(null, dateOfFirstResponseCFType.getJsonFromIssue(customField, new MockIssue(), true, mock(FieldLayoutItem.class)).getStandardData().getData());
    }

    public void testJsonContainsRenderedDataWhenItsRequested() {
        Timestamp timestamp = new Timestamp(1);
        when(dateOfFirstResponseDAO.calculateDate(any(Issue.class))).thenReturn(timestamp);
        assertEquals(Dates.asTimeString(new Date(timestamp.getTime())),
                dateOfFirstResponseCFType.getJsonFromIssue(customField, new MockIssue(), true, mock(FieldLayoutItem.class)).getStandardData().getData().toString());
        assertEquals(new DateTimeFormatterFactoryStub().formatter().forLoggedInUser().format(new Date(timestamp.getTime())),
                dateOfFirstResponseCFType.getJsonFromIssue(customField, new MockIssue(), true, mock(FieldLayoutItem.class)).getRenderedData().getData().toString());
    }

    public void testJsonDoesNotContainRenderedDataWhenItsNotRequested() {
        Timestamp timestamp = new Timestamp(1);
        when(dateOfFirstResponseDAO.calculateDate(any(Issue.class))).thenReturn(timestamp);
        assertEquals(Dates.asTimeString(new Date(timestamp.getTime())),
                dateOfFirstResponseCFType.getJsonFromIssue(customField, new MockIssue(), true, mock(FieldLayoutItem.class)).getStandardData().getData().toString());
        assertEquals(null, dateOfFirstResponseCFType.getJsonFromIssue(customField, new MockIssue(), false, mock(FieldLayoutItem.class)).getRenderedData());
    }
}
