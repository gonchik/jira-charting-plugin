package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfo;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfoContext;
import com.atlassian.jira.issue.fields.rest.RestAwareCustomFieldType;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.rest.Dates;
import org.ofbiz.core.entity.DelegatorInterface;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

/**
 * This class is a custom field type which measures the date that the issue had its first response.
 *
 * The first response is the first comment that is not by the reporter of the issue.
 */
public class DateOfFirstResponseCFType extends AbstractCalculatedDateCFType implements DateField, RestAwareCustomFieldType
{
    private final DateOfFirstResponseDAO dateOfFirstResponseDAO;

    private final DateTimeFormatter datePickerFormatter;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;

    public DateOfFirstResponseCFType(DatePickerConverter dateConverter, CustomFieldValuePersister customFieldValuePersister, DateTimeFormatterFactory dateTimeFormatterFactory)
    {
        super(dateConverter, customFieldValuePersister);
        this.dateOfFirstResponseDAO = createDateOfFirstResponseDAO();
        this.datePickerFormatter = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_TIME_PICKER);
        this.dateTimeFormatterFactory = dateTimeFormatterFactory;
    }

    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    {
        Map<String, Object> context = super.getVelocityParameters(issue, field, fieldLayoutItem);

        context.put("datePickerFormatter", datePickerFormatter);
        context.put("titleFormatter", datePickerFormatter.withStyle(DateTimeStyle.COMPLETE));
        context.put("iso8601Formatter", datePickerFormatter.withStyle(DateTimeStyle.ISO_8601_DATE_TIME));

        return context;
    }

    protected DateOfFirstResponseDAO createDateOfFirstResponseDAO()
    {
        return new DateOfFirstResponseDAO(ComponentAccessor.getComponent(DelegatorInterface.class));
    }

    public Timestamp getValueFromIssue(CustomField field, Issue issue)
    {
        // The first response date should not come from the database, since it is based on the first comment
        // by a user who is not the issue reporter and that is a state that can change. For instance, the first
        // comment gets deleted or the issue reporter changed.
        // Since it can't be read from the dbase, we don't need to store the value into it, saving one db operation
        // and fixing https://studio.plugins.atlassian.com/browse/JCHART-357
        return calculateFirstResponseDate(issue);
    }

    protected Timestamp calculateFirstResponseDate(Issue issue)
    {
        // NB - Cannot use ActionManager here, as that only shows comments valid for a specific user
        return dateOfFirstResponseDAO.calculateDate(issue);
    }

    @Override
    public FieldTypeInfo getFieldTypeInfo(final FieldTypeInfoContext fieldTypeInfoContext) {
        return new FieldTypeInfo(null, null);
    }

    @Override
    public JsonType getJsonSchema(final CustomField customField) {
        return JsonTypeBuilder.custom(JsonType.DATETIME_TYPE, getKey(), customField.getIdAsLong());
    }

    @Override
    public FieldJsonRepresentation getJsonFromIssue(final CustomField customField, final Issue issue, final boolean renderedVersionRequested, final FieldLayoutItem fieldLayoutItem)
    {
        Timestamp timestamp = calculateFirstResponseDate(issue);
        if (timestamp == null)
        {
            return new FieldJsonRepresentation(new JsonData(null));
        }
        Date date = new Date(timestamp.getTime());
        FieldJsonRepresentation pair = new FieldJsonRepresentation(new JsonData(Dates.asTimeString(date)));
        if (renderedVersionRequested)
        {
            pair.setRenderedData(new JsonData(dateTimeFormatterFactory.formatter().forLoggedInUser().format(date)));
        }
        return pair;
    }
}
