package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.jdbc.ReadOnlySQLProcessor;
import org.ofbiz.core.entity.jdbc.SQLProcessor;

import java.sql.ResultSet;
import java.sql.Timestamp;

/**
 * A DAO that will return a single Timestamp.
 * <p>
 * Developers sub-classing this class need to override {@link #getSelectStatement()} and
 * return a select statement
 */
public abstract class AbstractSingleDateDAO
{
    String ENTITY_DS = "defaultDS"; // this is package private so that it can be overridden in tests
    private static final Logger log = Logger.getLogger(AbstractSingleDateDAO.class);
    protected final DelegatorInterface delegatorInterface;

    protected AbstractSingleDateDAO(DelegatorInterface delegatorInterface)
    {
        this.delegatorInterface = delegatorInterface;
    }

    /**
     * This is a select statement, that takes one parameter (an issue id), and returns a single Timestamp column
     *
     * @return A select statement that returns a single column, the date field that you wish
     */
    protected abstract String getSelectStatement();

    protected String getTableName(String entityName)
    {
        return delegatorInterface.getModelEntity(entityName).getTableName(ENTITY_DS);
    }

    protected String getColName(String entityName, String fieldName)
    {
        return delegatorInterface.getModelEntity(entityName).getField(fieldName).getColName();
    }

    public Timestamp calculateDate(Issue issue)
    {
        Timestamp dateResolved = null;
        SQLProcessor sqlProcessor = null;
        ResultSet resultSet;
        try
        {
            sqlProcessor = new ReadOnlySQLProcessor(ENTITY_DS);
            sqlProcessor.prepareStatement(getSelectStatement());
            sqlProcessor.setValue(issue.getId());

            sqlProcessor.executeQuery();
            resultSet = sqlProcessor.getResultSet();

            if (resultSet.next())
            {
                dateResolved = resultSet.getTimestamp(1);
            }
        }
        catch (Throwable e)
        {
            log.error(e, e);
        }
        finally
        {
            if (sqlProcessor != null)
            {
                try
                {
                    // Closing the SQLProcessor closes the ResultSet as well.
                    sqlProcessor.close();
                }
                catch (Exception e)
                {
                    log.error(e, e);
                }
            }
        }
        return dateResolved;
    }
}
